# Microbrasseries du Quebec

J'etais tanne de faire des recherches dans Google Map avec `brewery`, `microbrasseries`, `microbrewery`, etc.. et toujours avoir des resultats differents.   De plus, j'obtenais des restaurants aussi et ce n'est pas ce qui m'interesse.

J'ai trouver ce [site](jpbarbo.ca) qui contient, a ce qu'il me semble, une liste complete des microbrasseries du Quebec!

J'ai donc utilise ces donnees pour generer ce [fichier](./microbrasseries.csv).

# Usage

Il y a fort probablement different facon d'utiliser les donnees.   Voici un example :

## Google My Map

* Ouvrir `Google My Map`
* `Add Layer` + `import` le fichier `microbrasseries.csv`
* Choisir les options `Styled by region` & `Set labels` **Microbrasseries**

### Example

![Google Map](images/gmap.png)

Une fois importe, vous pouvez l'ajouter comme un `overlay` dans Google Map (PC) ou mobile (ie: IOS).  De cette facon, vous pourrez utiliser la navigation normalement..

#### PC

* Your places
* Maps
* Microbrasseries du Quebec

#### IOS

* Saved
* Maps
* Microbrasseries du Quebec

# Contribution

Pull requests accepted! :-) 

